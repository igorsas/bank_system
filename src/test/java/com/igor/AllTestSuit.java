package com.igor;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit run everything")
@SelectPackages({"com.igor.controller", "com.igor.view", "com.igor"})
@IncludeClassNamePatterns({"\\b.*\\b"})
public class AllTestSuit {
}