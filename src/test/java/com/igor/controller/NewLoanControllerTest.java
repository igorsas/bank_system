package com.igor.controller;

import com.igor.controller.create.NewLoanController;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NewLoanControllerTest {
    @Test
    void checkPercent() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class clazz = NewLoanController.class;
        NewLoanController controller = new NewLoanController();
        Method method = clazz.getDeclaredMethod("checkPercent", String.class);
        method.setAccessible(true);
        assertFalse((Boolean) method.invoke(controller, "10"));
        assertTrue((Boolean) method.invoke(controller, "15"));
        assertFalse((Boolean) method.invoke(controller, "20"));
    }
}