package com.igor.controller;

import com.igor.controller.create.NewAccountController;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NewAccountControllerTest {
    @Test
    void checkPercent() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class clazz = NewAccountController.class;
        NewAccountController controller = new NewAccountController();
        Method method = clazz.getDeclaredMethod("checkPercent", String.class);
        method.setAccessible(true);
        assertFalse((Boolean) method.invoke(controller, "5"));
        assertTrue((Boolean) method.invoke(controller, "10"));
        assertFalse((Boolean) method.invoke(controller, "15"));
    }
}