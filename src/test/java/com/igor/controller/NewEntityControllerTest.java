package com.igor.controller;

import com.igor.controller.create.NewAccountController;
import com.igor.controller.create.NewLoanController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NewEntityControllerTest {
    @Test
    void checkPeriod() {
        assertFalse(new NewLoanController().checkPeriod("5"));
        assertTrue(new NewAccountController().checkPeriod("6"));
    }
}