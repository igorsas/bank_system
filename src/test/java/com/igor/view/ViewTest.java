package com.igor.view;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ViewTest {

    @Test
    void isInteger() {
        assertTrue(View.isInteger("5"));
        assertFalse(View.isInteger("afhb"));
        assertFalse(View.isInteger("-5"));
    }

    @Test
    void isDouble() {
        assertTrue(View.isDouble("5.46812"));
        assertFalse(View.isDouble("ahfjd"));
        assertFalse(View.isDouble("-5.23"));
    }
}