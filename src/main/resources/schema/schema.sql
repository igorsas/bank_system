-- -----------------------------------------------------
-- Schema bank
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bank` DEFAULT CHARACTER SET utf8;
USE `bank`;

-- -----------------------------------------------------
-- Table `bank`.`customer_name`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`customer_name`
(
  `id`          INT         NOT NULL auto_increment,
  `first_name`  VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NOT NULL,
  `last_name`   VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
)
  ENGINE = InnoDB;

INSERT INTO `bank`.`customer_name`
VALUES (1, 'Igor', 'Mykhailovych', 'Sas'),
       (2, 'Alex', 'Olexandrovych', 'Tsupanych');

-- -----------------------------------------------------
-- Table `bank`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`customer`
(
  `id`               INT NOT NULL auto_increment,
  `customer_name_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_customer_customer_name_idx` (`customer_name_id` ASC),
  CONSTRAINT `fk_customer_customer_name`
    FOREIGN KEY (`customer_name_id`)
      REFERENCES `bank`.`customer_name` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

INSERT INTO `bank`.`customer`
VALUES (1, 1),
       (2, 2);
-- -----------------------------------------------------
-- Table `bank`.`bank`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`bank`
(
  `id`     INT         NOT NULL auto_increment,
  `assets` INT         NOT NULL,
  `name`   VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
)
  ENGINE = InnoDB;

INSERT INTO `bank`.`bank`
VALUES (1, 100000000, 'Oshchad Bank'),
       (2, 120000000, 'Pryvat Bank');

-- -----------------------------------------------------
-- Table `bank`.`loan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`loan`
(
  `id`              INT            NOT NULL auto_increment,
  `amount`          INT            NOT NULL,
  `percent`         DECIMAL(15, 2) NOT NULL,
  `name`            VARCHAR(45)    NOT NULL,
  `period_in_month` INT            NOT NULL,
  `bank_id`         INT            NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_loan_bank1_idx` (`bank_id` ASC),
  CONSTRAINT `fk_loan_bank1`
    FOREIGN KEY (`bank_id`)
      REFERENCES `bank`.`bank` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

INSERT INTO `bank`.`loan`
VALUES (1, 100, 18, 'For buying food', 1, 1),
       (2, 6000, 17, 'For little trip', 6, 2);

-- -----------------------------------------------------
-- Table `bank`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`account`
(
  `id`              INT            NOT NULL,
  `balance`         INT            NOT NULL,
  `percent`         DECIMAL(15, 2) NOT NULL,
  `name`            VARCHAR(45)    NOT NULL,
  `period_in_month` INT            NOT NULL,
  `bank_id`         INT            NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_account_bank1_idx` (`bank_id` ASC),
  CONSTRAINT `fk_account_bank1`
    FOREIGN KEY (`bank_id`)
      REFERENCES `bank`.`bank` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

insert into `bank`.`account`
values (1, 2000, 11, 'Pension fund', 100, 1),
       (2, 6000, 13, 'For studying', 30, 2);
-- -----------------------------------------------------
-- Table `bank`.`customer_has_loan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`customer_has_loan`
(
  `customer_id` INT NOT NULL,
  `loan_id`     INT NOT NULL,
  PRIMARY KEY (`customer_id`, `loan_id`),
  INDEX `fk_customer_has_loan_loan1_idx` (`loan_id` ASC),
  INDEX `fk_customer_has_loan_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_loan_customer1`
    FOREIGN KEY (`customer_id`)
      REFERENCES `bank`.`customer` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_loan_loan1`
    FOREIGN KEY (`loan_id`)
      REFERENCES `bank`.`loan` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

insert into `bank`.`customer_has_loan`
values (1, 1),
       (2, 2);
-- -----------------------------------------------------
-- Table `bank`.`customer_has_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`customer_has_account`
(
  `customer_id` INT NOT NULL,
  `account_id`  INT NOT NULL,
  PRIMARY KEY (`customer_id`, `account_id`),
  INDEX `fk_customer_has_account_account1_idx` (`account_id` ASC),
  INDEX `fk_customer_has_account_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_account_customer1`
    FOREIGN KEY (`customer_id`)
      REFERENCES `bank`.`customer` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_account_account1`
    FOREIGN KEY (`account_id`)
      REFERENCES `bank`.`account` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

insert into `bank`.`customer_has_account`
values (1, 2),
       (2, 1);