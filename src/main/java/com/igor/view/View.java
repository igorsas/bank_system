package com.igor.view;

import com.igor.controller.Controller;
import com.igor.controller.create.NewAccountController;
import com.igor.controller.create.NewBankController;
import com.igor.controller.create.NewLoanController;
import com.igor.controller.create.NewUserController;
import com.igor.controller.delete.DeleteAccountController;
import com.igor.controller.delete.DeleteLoanController;
import com.igor.controller.select.SelectUsersController;
import com.igor.controller.select.ShowInfoController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;

import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.SQLQueryConsts.SELECT_ALL_FROM_USER_AND_USER_NAME;
import static com.igor.model.constant.ViewConst.MENU_STR;
import static com.igor.model.constant.ViewConst.SELECT_MENU_POINT;

public class View {
    private static final Logger LOG = LogManager.getLogger(View.class);
    public static Integer userID = null;
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    public View() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        putInstructions();
        putRealizations();
    }

    public static Set<Integer> showUsers(String selectAllFromUserAndUserName) throws SQLException {
        Set<Integer> usersId = new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(selectAllFromUserAndUserName)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid - " + resultSet.getInt(1) +
                            "\nfirst_name - " + resultSet.getString(2) +
                            "\nmiddle_name - " + resultSet.getString(3) +
                            "\nlast_name - " + resultSet.getString(4));
                    usersId.add(resultSet.getInt(1));
                }
            }
        }
        return usersId;
    }

    public static Set<Integer> showBanks(String selectAllFromBank) throws SQLException {
        Set<Integer> banksId = new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(selectAllFromBank)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid - " + resultSet.getInt(1) +
                            "\nassets - " + resultSet.getString(2) +
                            "\nname - " + resultSet.getString(3));
                    banksId.add(resultSet.getInt(1));
                }
            }
        }
        return banksId;
    }

    public static void setUserID(Integer userID) {
        View.userID = userID;
    }

    public static boolean isInteger(String str) {
        String integerPattern = "[0-9]*";
        return Pattern.matches(integerPattern, str);
    }

    public static boolean isDouble(String str) {
        String decimalPattern = "([0-9]*)\\.([0-9]*)";
        return Pattern.matches(decimalPattern, str);
    }

    private void outputMenu(final Map<String, String> menu) {
        LOG.info(MENU_STR);
        menu.values().forEach(LOG::info);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu(menu);
            LOG.info(SELECT_MENU_POINT);
            try {
                String tmp = INPUT.readLine().toUpperCase();
                keyMenu = tmp.equals("") ? "Illegal argument" : tmp;
                if (keyMenu.equals("Q")) {
                    break;
                } else if (!isInteger(keyMenu)) {
                    LOG.info("Please choose correct option");
                    continue;
                } else if (Integer.parseInt(keyMenu) > methodsMenu.size() || Integer.parseInt(keyMenu) <= 0) {
                    LOG.info("Please choose correct option");
                    continue;
                }
                methodsMenu.get(keyMenu).execute();
            } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | IOException | SQLException e) {
                LOG.error(e.getClass());
                LOG.error(e.getMessage());
                LOG.trace(e.getStackTrace());
            }
        } while (true);
    }

    private void putInstructions() {
        int point = STARTED_POINT;
        menu.put(String.valueOf(point), String.format("%s - Show current user", point++));
        menu.put(String.valueOf(point), String.format("%s - Show info", point++));
        menu.put(String.valueOf(point), String.format("%s - Choose yourself", point++));
        menu.put(String.valueOf(point), String.format("%s - New user", point++));
        menu.put(String.valueOf(point), String.format("%s - New bank", point++));
        menu.put(String.valueOf(point), String.format("%s - New loan", point++));
        menu.put(String.valueOf(point), String.format("%s - New account", point++));
        menu.put(String.valueOf(point), String.format("%s - Delete loan", point++));
        menu.put(String.valueOf(point), String.format("%s - Delete account", point));
        menu.put("Q", "Q - exit");
    }

    private void putRealizations() {
        int point = STARTED_POINT;
        methodsMenu.put(String.valueOf(point++), this::showCurrentUser);
        methodsMenu.put(String.valueOf(point++), new ShowInfoController());
        methodsMenu.put(String.valueOf(point++), new SelectUsersController());
        methodsMenu.put(String.valueOf(point++), new NewUserController());
        methodsMenu.put(String.valueOf(point++), new NewBankController());
        methodsMenu.put(String.valueOf(point++), new NewLoanController());
        methodsMenu.put(String.valueOf(point++), new NewAccountController());
        methodsMenu.put(String.valueOf(point++), new DeleteLoanController());
        methodsMenu.put(String.valueOf(point), new DeleteAccountController());
    }

    private void showCurrentUser() throws SQLException {
        if (Objects.isNull(userID)) {
            LOG.info("We haven't got current user yet.");
        } else {
            LOG.info("Current user: ");
            showUsers(SELECT_ALL_FROM_USER_AND_USER_NAME + " where customer.id = " + userID);
        }
    }
}
