package com.igor.persistant;

import com.igor.model.property.Property;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import static com.igor.model.constant.PropertyConst.*;

public final class ConnectionManager {
    private static final Logger LOG = LogManager.getLogger(ConnectionManager.class);
    private static final String URL = Property.getProperty(URL_KEY);
    private static final String USER = Property.getProperty(USER_KEY);
    private static final String PASSWORD = Property.getProperty(PASSWORD_KEY);
    private static Connection connection;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (Objects.isNull(connection)) {
            try {
                connection = DriverManager.getConnection(Objects.requireNonNull(URL), USER, PASSWORD);
            } catch (SQLException e) {
                LOG.error(e.getMessage());
                LOG.error(e.getSQLState());
                LOG.error(e.getErrorCode());
            }
        }
        return connection;
    }
}
