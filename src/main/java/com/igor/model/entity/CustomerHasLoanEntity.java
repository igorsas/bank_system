package com.igor.model.entity;

public class CustomerHasLoanEntity {
    private CustomerHasLoanCompositePrimaryKey id;

    public CustomerHasLoanEntity(int customerID, int loanID) {
        id = new CustomerHasLoanCompositePrimaryKey(customerID, loanID);
    }

    public CustomerHasLoanEntity(CustomerHasLoanCompositePrimaryKey id) {
        this.id = id;
    }

    public CustomerHasLoanCompositePrimaryKey getId() {
        return id;
    }

    public void setId(CustomerHasLoanCompositePrimaryKey id) {
        this.id = id;
    }

    public int getCustomerId() {
        return id.getCustomerID();
    }

    public void setCustomerId(int customerId) {
        id.setCustomerID(customerId);
    }

    public int getLoanId() {
        return id.getLoanID();
    }

    public void setLoanId(int loanId) {
        id.setLoanID(loanId);
    }
}
