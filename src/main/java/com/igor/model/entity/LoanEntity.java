package com.igor.model.entity;

public class LoanEntity {
    private int id;
    private int amount;
    private double percent;
    private String name;
    private int periodInMonth;
    private int bankId;

    public LoanEntity(int id, int amount, double percent, String name, int periodInMonth, int bankId) {
        this.id = id;
        this.amount = amount;
        this.percent = percent;
        this.name = name;
        this.periodInMonth = periodInMonth;
        this.bankId = bankId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPeriodInMonth() {
        return periodInMonth;
    }

    public void setPeriodInMonth(int periodInMonth) {
        this.periodInMonth = periodInMonth;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }
}
