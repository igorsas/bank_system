package com.igor.model.entity;

public class CustomerHasLoanCompositePrimaryKey {
    private int customerID;
    private int loanID;

    public CustomerHasLoanCompositePrimaryKey(int customerID, int loanID) {
        this.customerID = customerID;
        this.loanID = loanID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getLoanID() {
        return loanID;
    }

    public void setLoanID(int loanID) {
        this.loanID = loanID;
    }
}
