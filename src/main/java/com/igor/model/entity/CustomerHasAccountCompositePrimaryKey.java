package com.igor.model.entity;

public class CustomerHasAccountCompositePrimaryKey {
    private int customerID;
    private int accountID;

    public CustomerHasAccountCompositePrimaryKey(int customerID, int accountID) {
        this.customerID = customerID;
        this.accountID = accountID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }
}
