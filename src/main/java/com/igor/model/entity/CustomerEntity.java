package com.igor.model.entity;

public class CustomerEntity {
    private int id;
    private int customerNameId;

    public CustomerEntity(int id, int customerNameId) {
        this.id = id;
        this.customerNameId = customerNameId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerNameId() {
        return customerNameId;
    }

    public void setCustomerNameId(int customerNameId) {
        this.customerNameId = customerNameId;
    }
}
