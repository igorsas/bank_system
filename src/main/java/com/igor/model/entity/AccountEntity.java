package com.igor.model.entity;

public class AccountEntity {
    private int id;
    private int balance;
    private double percent;
    private String name;
    private int periodInMonth;
    private int bankId;

    public AccountEntity(int id, int balance, double percent, String name, int periodInMonth, int bankId) {
        this.id = id;
        this.balance = balance;
        this.percent = percent;
        this.name = name;
        this.periodInMonth = periodInMonth;
        this.bankId = bankId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPeriodInMonth() {
        return periodInMonth;
    }

    public void setPeriodInMonth(int periodInMonth) {
        this.periodInMonth = periodInMonth;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }
}
