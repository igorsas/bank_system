package com.igor.model.entity;

public class BankEntity {
    private int id;
    private int assets;
    private String name;

    public BankEntity(int id, int assets, String name) {
        this.id = id;
        this.assets = assets;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssets() {
        return assets;
    }

    public void setAssets(int assets) {
        this.assets = assets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
