package com.igor.model.entity;

public class CustomerHasAccountEntity {
    private CustomerHasAccountCompositePrimaryKey id;

    public CustomerHasAccountEntity(int customerID, int accountID) {
        id = new CustomerHasAccountCompositePrimaryKey(customerID, accountID);
    }

    public CustomerHasAccountEntity(CustomerHasAccountCompositePrimaryKey id) {
        this.id = id;
    }

    public CustomerHasAccountCompositePrimaryKey getId() {
        return id;
    }

    public void setId(CustomerHasAccountCompositePrimaryKey id) {
        this.id = id;
    }

    public int getCustomerId() {
        return id.getCustomerID();
    }

    public void setCustomerId(int customerId) {
        id.setCustomerID(customerId);
    }

    public int getAccountId() {
        return id.getAccountID();
    }

    public void setAccountId(int accountId) {
        id.setAccountID(accountId);
    }
}
