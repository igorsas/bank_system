package com.igor.model.constant;

public final class ViewConst {
    public static final String SELECT_MENU_POINT = "Please, select menu point.";
    public static final String MENU_STR = "\nMENU:";

    private ViewConst() {
    }
}
