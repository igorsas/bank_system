package com.igor.model.constant;

public final class PropertyConst {
    public static final String PATH_TO_DB_PROPERTIES = "property/db.properties";
    public static final String URL_KEY = "url";
    public static final String USER_KEY = "user";
    public static final String PASSWORD_KEY = "password";

    private PropertyConst() {
    }
}
