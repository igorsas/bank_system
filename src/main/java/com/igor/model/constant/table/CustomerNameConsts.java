package com.igor.model.constant.table;

public class CustomerNameConsts {
    public final static String TABLE_NAME = "customer_name";
    public final static String ID = "id";
    public final static String FIRST_NAME = "first_name";
    public final static String MIDDLE_NAME = "middle_name";
    public final static String LAST_NAME = "last_name";

    private CustomerNameConsts() {
    }
}
