package com.igor.model.constant.table;

public class LoanConsts {
    public final static String TABLE_NAME = "loan";
    public final static String ID = "id";
    public final static String AMOUNT = "amount";
    public final static String PERCENT = "percent";
    public final static String NAME = "name";
    public final static String PERIOD_IN_MONTH = "period_in_month";
    public final static String BANK_ID = "bank_id";

    private LoanConsts() {
    }
}
