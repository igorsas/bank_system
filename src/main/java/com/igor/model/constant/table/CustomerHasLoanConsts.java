package com.igor.model.constant.table;

public class CustomerHasLoanConsts {
    public final static String TABLE_NAME = "customer_has_loan";
    public final static String CUSTOMER_ID = "customer_id";
    public final static String LOAN_ID = "loan_id";

    private CustomerHasLoanConsts() {
    }
}
