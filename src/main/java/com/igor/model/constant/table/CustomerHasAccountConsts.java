package com.igor.model.constant.table;

public class CustomerHasAccountConsts {
    public final static String TABLE_NAME = "customer_has_account";
    public final static String CUSTOMER_ID = "customer_id";
    public final static String ACCOUNT_ID = "account_id";

    private CustomerHasAccountConsts() {
    }
}
