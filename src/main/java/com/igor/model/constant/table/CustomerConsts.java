package com.igor.model.constant.table;

public class CustomerConsts {
    public final static String TABLE_NAME = "customer";
    public final static String ID = "id";
    public final static String CUSTOMER_NAME_ID = "customer_name_id";

    private CustomerConsts() {
    }
}
