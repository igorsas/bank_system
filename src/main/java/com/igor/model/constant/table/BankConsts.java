package com.igor.model.constant.table;

public class BankConsts {
    public final static String TABLE_NAME = "bank";
    public final static String ID = "id";
    public final static String ASSETS = "assets";
    public final static String NAME = "name";

    private BankConsts() {
    }
}
