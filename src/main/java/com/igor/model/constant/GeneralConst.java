package com.igor.model.constant;

import com.igor.persistant.ConnectionManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;

public final class GeneralConst {
    public static final int STARTED_POINT = 1;

    public static final BufferedReader INPUT = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
    public static final Connection CONNECTION = ConnectionManager.getConnection();

    private GeneralConst() {
    }
}
