package com.igor.model.constant;

public class SQLQueryConsts {
    public static final String DELETE_GENERAL = "DELETE FROM %s WHERE %s=?";

    public static final String DELETE_LINE_FROM_INTERMEDIATE_TABLE = "DELETE FROM %s WHERE %s=? AND %s=?";
    public static final String UPDATE_LINE_FROM_INTERMEDIATE_TABLE = "INSERT %s (%s, %s) VALUES (?, ?)";

    public final static String SELECT_ALL_FROM_BANK = "SELECT * FROM bank";
    public final static String SELECT_MONEY_FROM_BANK = "SELECT assets FROM bank where id = ?";
    public static final String INSERT_ALL_INFO_IN_BANK = "INSERT %s (%s, %s, %s) VALUES (?, ?, ?)";
    public static final String UPDATE_ALL_INFO_IN_BANK = "UPDATE %s SET %s=?, %s=? WHERE %s=?";
    public static final String UPDATE_ASSETS_IN_BANK = "UPDATE %s SET %s=? WHERE %s=?";
    public static final String SELECT_ASSETS_IN_BANK = "SELECT %s FROM %s WHERE %s=?";

    public final static String SELECT_BANK_ID_FROM_LOAN = "select loan.bank_id from loan where id = ?";
    public static final String SELECT_LOAN_INFO_FOR_GIVING_MONEY = "select loan.amount, loan.percent, loan.period_in_month from loan where loan.id = ?";
    public static final String INSERT_ALL_INFO_IN_LOAN = "INSERT %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_ALL_INFO_IN_LOAN = "UPDATE %s SET %s=?, %s=?, %s=?, %s=?, %s=? WHERE %s=?";

    public static final String SELECT_BANK_ID_FROM_ACCOUNT = "select account.bank_id from account where id = ?";
    public static final String SELECT_ACCOUNT_INFO_FOR_GIVING_MONEY = "select account.balance, account.percent, account.period_in_month from account where account.id = ?";
    public static final String INSERT_ALL_INFO_IN_ACCOUNT = "INSERT %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_ALL_INFO_IN_ACCOUNT = "UPDATE %s SET %s=?, %s=?, %s=?, %s=?, %s=? WHERE %s=?";

    public static final String INSERT_ALL_INFO_IN_CUSTOMER = "INSERT %s (%s, %s) VALUES (?, ?)";
    public static final String UPDATE_ALL_INFO_IN_CUSTOMER = "UPDATE %s SET %s=? WHERE %s=?";
    public static final String INSERT_ALL_INFO_IN_CUSTOMER_NAME = "INSERT %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_ALL_INFO_IN_CUSTOMER_NAME = "UPDATE %s SET %s=?, %s=?, %s=? WHERE %s=?";

    public static final String SELECT_ACCOUNT_INFO_FOR_CHECKING_POSSIBILITY_GIVE_MONEY = "select bank.assets, account.balance, account.percent, account.period_in_month from bank join account on bank.id = account.bank_id where account.id = ?";
    public static final String SELECT_ALL_FROM_USER_AND_USER_NAME = "select customer.id, customer_name.first_name, customer_name.middle_name, customer_name.last_name from customer join customer_name on customer_name.id = customer.customer_name_id";
    public final static String SELECT_ACCOUNT_INFO = "select account.id, account.balance, account.percent, account.name, account.period_in_month, bank.name from account join customer_has_account on customer_has_account.account_id = account.id join bank on account.bank_id = bank.id where customer_has_account.customer_id = ? group by account.id";
    public final static String SELECT_LOAN_INFO = "select loan.id, loan.amount, loan.percent, loan.name, loan.period_in_month, bank.name from loan join customer_has_loan on loan.id = customer_has_loan.loan_id join bank on loan.bank_id = bank.id where customer_has_loan.customer_id = ? group by loan.id";

    private SQLQueryConsts() {
    }
}
