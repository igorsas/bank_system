package com.igor.dao.implementation;

import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.entity.CustomerEntity;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.SQLQueryConsts.*;
import static com.igor.model.constant.table.CustomerConsts.*;

public class CustomerDao implements GeneralDAO<CustomerEntity, Integer> {
    private static final String CREATE = String.format(INSERT_ALL_INFO_IN_CUSTOMER, TABLE_NAME, ID, CUSTOMER_NAME_ID);
    private static final String UPDATE = String.format(UPDATE_ALL_INFO_IN_CUSTOMER, TABLE_NAME, CUSTOMER_NAME_ID, ID);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);

    @Override
    public int create(CustomerEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setInt(2, entity.getCustomerNameId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(CustomerEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getCustomerNameId());
            preparedStatement.setInt(2, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
