package com.igor.dao.implementation;

import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.entity.AccountEntity;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.SQLQueryConsts.*;
import static com.igor.model.constant.table.AccountConsts.*;

public class AccountDao implements GeneralDAO<AccountEntity, Integer> {
    private static final String CREATE = String.format(INSERT_ALL_INFO_IN_ACCOUNT, TABLE_NAME, ID, BALANCE, PERCENT, NAME, PERIOD_IN_MONTH, BANK_ID);
    private static final String UPDATE = String.format(UPDATE_ALL_INFO_IN_ACCOUNT, TABLE_NAME, BALANCE, PERCENT, NAME, PERIOD_IN_MONTH, BANK_ID, ID);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);


    @Override
    public int create(AccountEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setInt(2, entity.getBalance());
            preparedStatement.setDouble(3, entity.getPercent());
            preparedStatement.setString(4, entity.getName());
            preparedStatement.setInt(5, entity.getPeriodInMonth());
            preparedStatement.setInt(6, entity.getBankId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(AccountEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getBalance());
            preparedStatement.setDouble(2, entity.getPercent());
            preparedStatement.setString(3, entity.getName());
            preparedStatement.setInt(4, entity.getPeriodInMonth());
            preparedStatement.setInt(5, entity.getBankId());
            preparedStatement.setInt(6, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
