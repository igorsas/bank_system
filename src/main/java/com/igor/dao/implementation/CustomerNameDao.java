package com.igor.dao.implementation;

import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.entity.CustomerNameEntity;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.SQLQueryConsts.*;
import static com.igor.model.constant.table.CustomerNameConsts.*;

public class CustomerNameDao implements GeneralDAO<CustomerNameEntity, Integer> {
    private static final String CREATE = String.format(INSERT_ALL_INFO_IN_CUSTOMER_NAME, TABLE_NAME, ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME);
    private static final String UPDATE = String.format(UPDATE_ALL_INFO_IN_CUSTOMER_NAME, TABLE_NAME, FIRST_NAME, MIDDLE_NAME, LAST_NAME, ID);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);

    @Override
    public int create(CustomerNameEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setString(2, entity.getFirstName());
            preparedStatement.setString(3, entity.getMiddleName());
            preparedStatement.setString(4, entity.getLastName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(CustomerNameEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getMiddleName());
            preparedStatement.setString(3, entity.getLastName());
            preparedStatement.setInt(4, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
