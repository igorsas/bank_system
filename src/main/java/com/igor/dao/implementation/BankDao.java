package com.igor.dao.implementation;

import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.entity.BankEntity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.SQLQueryConsts.*;
import static com.igor.model.constant.table.BankConsts.*;

public class BankDao implements GeneralDAO<BankEntity, Integer> {
    private static final String CREATE = String.format(INSERT_ALL_INFO_IN_BANK, TABLE_NAME, ID, ASSETS, NAME);
    private static final String UPDATE = String.format(UPDATE_ALL_INFO_IN_BANK, TABLE_NAME, ASSETS, NAME, ID);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String UPDATE_ASSETS = String.format(UPDATE_ASSETS_IN_BANK, TABLE_NAME, ASSETS, ID);
    private static final String SELECT_ASSETS = String.format(SELECT_ASSETS_IN_BANK, ASSETS, TABLE_NAME, ID);

    public void updateAssets(int id, int newAssets) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE_ASSETS)) {
            preparedStatement.setInt(1, newAssets);
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate();
        }
    }

    public int getAssetsById(Integer id)
            throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(SELECT_ASSETS)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                return resultSet.next() ? resultSet.getInt(1) : 0;
            }
        }
    }

    @Override
    public int create(BankEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setInt(2, entity.getAssets());
            preparedStatement.setString(3, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(BankEntity entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getAssets());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setInt(3, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
