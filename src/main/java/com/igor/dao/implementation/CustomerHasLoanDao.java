package com.igor.dao.implementation;

import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.entity.CustomerHasLoanCompositePrimaryKey;
import com.igor.model.entity.CustomerHasLoanEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.SQLQueryConsts.DELETE_LINE_FROM_INTERMEDIATE_TABLE;
import static com.igor.model.constant.SQLQueryConsts.UPDATE_LINE_FROM_INTERMEDIATE_TABLE;
import static com.igor.model.constant.table.CustomerHasLoanConsts.*;

public class CustomerHasLoanDao implements GeneralDAO<CustomerHasLoanEntity, CustomerHasLoanCompositePrimaryKey> {
    private static final Logger LOG = LogManager.getLogger(CustomerHasLoanDao.class);
    private static final String DELETE = String.format(DELETE_LINE_FROM_INTERMEDIATE_TABLE, TABLE_NAME, CUSTOMER_ID, LOAN_ID);
    private static final String CREATE = String.format(UPDATE_LINE_FROM_INTERMEDIATE_TABLE, TABLE_NAME, CUSTOMER_ID, LOAN_ID);

    @Override
    public int create(final CustomerHasLoanEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getCustomerId()));
            ps.setString(2, String.valueOf(entity.getLoanId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final CustomerHasLoanEntity entity) {
        LOG.info("This entity can't update! Only create or delete!");
        return 0;
    }

    @Override
    public int delete(final CustomerHasLoanCompositePrimaryKey id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id.getCustomerID()));
            ps.setString(2, String.valueOf(id.getLoanID()));
            return ps.executeUpdate();
        }
    }
}
