package com.igor.dao.interfaces;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;

public interface GeneralDAO<T, ID> {
    static int getNewId(String table) throws SQLException {
        try (PreparedStatement statement = CONNECTION.prepareStatement(String.format("SELECT COUNT(1) FROM %s", table))) {
            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? resultSet.getInt(1) + 1 : 0;
            }
        }
    }

    int create(T entity) throws SQLException;

    int update(T entity) throws SQLException;

    int delete(ID id) throws SQLException;
}
