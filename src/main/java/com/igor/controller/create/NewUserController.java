package com.igor.controller.create;

import com.igor.controller.Controller;
import com.igor.dao.implementation.CustomerDao;
import com.igor.dao.implementation.CustomerNameDao;
import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.constant.table.CustomerConsts;
import com.igor.model.constant.table.CustomerNameConsts;
import com.igor.model.entity.CustomerEntity;
import com.igor.model.entity.CustomerNameEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.INPUT;

public class NewUserController implements Controller {
    private static final Logger LOG = LogManager.getLogger(NewUserController.class);

    @Override
    public void execute() throws IOException, SQLException {
        LOG.info("Enter your first name:");
        String firstName = INPUT.readLine();
        LOG.info("Enter your middle name:");
        String middleName = INPUT.readLine();
        LOG.info("Enter your last name:");
        String lastName = INPUT.readLine();
        int newId = GeneralDAO.getNewId(CustomerNameConsts.TABLE_NAME);
        CustomerNameEntity name = new CustomerNameEntity(newId, firstName, middleName, lastName);
        new CustomerNameDao().create(name);
        CustomerEntity customer = new CustomerEntity(GeneralDAO.getNewId(CustomerConsts.TABLE_NAME), name.getId());
        new CustomerDao().create(customer);
    }
}
