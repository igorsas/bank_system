package com.igor.controller.create;

import com.igor.dao.implementation.BankDao;
import com.igor.dao.implementation.CustomerHasLoanDao;
import com.igor.dao.implementation.LoanDao;
import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.constant.table.LoanConsts;
import com.igor.model.entity.CustomerHasLoanEntity;
import com.igor.model.entity.LoanEntity;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.SELECT_MONEY_FROM_BANK;
import static com.igor.view.View.*;

public class NewLoanController implements NewEntityController {
    @Override
    public void execute() throws SQLException, IOException {
        if (Objects.isNull(userID)) {
            LOG.info("\nChoose yourself!!!");
        } else {
            int bankId = getBankId();
            LOG.info("Input name of loan:");
            String name = INPUT.readLine();
            LOG.info("Input percent of loan:");
            String percent = INPUT.readLine();
            LOG.info("Input amount of loan:");
            String amount = INPUT.readLine();
            LOG.info("Input period in month:");
            String periodInMonth = INPUT.readLine();
            if (checkBank(bankId, amount) && checkPercent(percent) && checkPeriod(periodInMonth)) {
                new LoanDao().create(new LoanEntity(GeneralDAO.getNewId(LoanConsts.TABLE_NAME), Integer.parseInt(amount),
                        Double.parseDouble(percent), name, Integer.parseInt(periodInMonth), bankId));
                new BankDao().updateAssets(bankId, new BankDao().getAssetsById(bankId) - Integer.parseInt(amount));
                new CustomerHasLoanDao().create(new CustomerHasLoanEntity(userID,
                        GeneralDAO.getNewId(LoanConsts.TABLE_NAME) - 1));
            } else {
                LOG.info("Something wrong! Check your input.");
            }
        }
    }

    private boolean checkBank(int bankID, String amount) throws SQLException {
        if (isInteger(amount)) {
            try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SELECT_MONEY_FROM_BANK)) {
                preparedStatement.setInt(1, bankID);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getInt(1) >= Integer.parseInt(amount);
                    }
                }
            }
        }
        return false;
    }

    private boolean checkPercent(String percent) {
        if (isDouble(percent) || isInteger(percent)) {
            return Math.abs(Double.parseDouble(percent) - 15) < 5;
        }
        return false;
    }
}
