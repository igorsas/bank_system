package com.igor.controller.create;

import com.igor.controller.Controller;
import com.igor.dao.implementation.BankDao;
import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.constant.table.BankConsts;
import com.igor.model.entity.BankEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.view.View.isInteger;

public class NewBankController implements Controller {
    private static final Logger LOG = LogManager.getLogger(NewBankController.class);

    @Override
    public void execute() throws SQLException, IOException {
        LOG.info("Enter bank name:");
        String name = INPUT.readLine();
        LOG.info("Enter bank assets:");
        String assetsStr;
        while (!isInteger(assetsStr = INPUT.readLine()) || Integer.parseInt(assetsStr) < 1000) {
            LOG.info("Enter bank assets:");
        }
        int assets = Integer.parseInt(assetsStr);
        BankEntity bank = new BankEntity(GeneralDAO.getNewId(BankConsts.TABLE_NAME), assets, name);
        new BankDao().create(bank);
    }
}
