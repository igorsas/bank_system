package com.igor.controller.create;

import com.igor.dao.implementation.AccountDao;
import com.igor.dao.implementation.BankDao;
import com.igor.dao.implementation.CustomerHasAccountDao;
import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.constant.table.AccountConsts;
import com.igor.model.entity.AccountEntity;
import com.igor.model.entity.CustomerHasAccountEntity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.view.View.*;

public class NewAccountController implements NewEntityController {
    @Override
    public void execute() throws SQLException, IOException {
        if (Objects.isNull(userID)) {
            LOG.info("\nChoose yourself!!!");
        } else {
            int bankId = getBankId();
            LOG.info("Input name of account:");
            String name = INPUT.readLine();
            LOG.info("Input percent of account:");
            String percent = INPUT.readLine();
            LOG.info("Input balance of account:");
            String balance = INPUT.readLine();
            LOG.info("Input period in month:");
            String periodInMonth = INPUT.readLine();
            if (checkPercent(percent) && checkPeriod(periodInMonth)) {
                new AccountDao().create(new AccountEntity(GeneralDAO.getNewId(AccountConsts.TABLE_NAME), Integer.parseInt(balance),
                        Double.parseDouble(percent), name, Integer.parseInt(periodInMonth), bankId));
                new BankDao().updateAssets(bankId, new BankDao().getAssetsById(bankId) + Integer.parseInt(balance));
                new CustomerHasAccountDao().create(new CustomerHasAccountEntity(userID,
                        GeneralDAO.getNewId(AccountConsts.TABLE_NAME) - 1));
            } else {
                LOG.info("Something wrong! Check your input.");
            }


        }
    }

    private boolean checkPercent(String percent) {
        if (isDouble(percent) || isInteger(percent)) {
            return Math.abs(Double.parseDouble(percent) - 10) < 5;
        }
        return false;
    }
}
