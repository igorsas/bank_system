package com.igor.controller.create;

import com.igor.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.SELECT_ALL_FROM_BANK;
import static com.igor.view.View.isInteger;
import static com.igor.view.View.showBanks;

public interface NewEntityController extends Controller {
    Logger LOG = LogManager.getLogger(NewEntityController.class);

    default int getBankId() throws SQLException, IOException {
        while (true) {
            LOG.info("Choose bank id: ");
            Set<Integer> banksId = showBanks(SELECT_ALL_FROM_BANK);
            if (!banksId.isEmpty()) {
                String bankIdStr = INPUT.readLine();
                if (isInteger(bankIdStr)) {
                    if (banksId.contains(Integer.parseInt(bankIdStr))) {
                        return Integer.parseInt(bankIdStr);
                    } else {
                        LOG.info("Choose correct option");
                    }
                }
            }
        }
    }

    default boolean checkPeriod(String period) {
        if (isInteger(period)) {
            return Integer.parseInt(period) >= 6;
        }
        return false;
    }
}
