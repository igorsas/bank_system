package com.igor.controller.select;

import com.igor.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.SELECT_ACCOUNT_INFO;
import static com.igor.model.constant.SQLQueryConsts.SELECT_LOAN_INFO;
import static com.igor.view.View.isInteger;
import static com.igor.view.View.userID;

public class ShowInfoController implements Controller {
    private static final Logger LOG = LogManager.getLogger(ShowInfoController.class);

    public static Set<Integer> showInfoAboutLoans() throws SQLException {
        Set<Integer> loansId = new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SELECT_LOAN_INFO)) {
            preparedStatement.setInt(1, userID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid - " + resultSet.getInt(1) +
                            "\namount - " + resultSet.getInt(2) +
                            "\npercent - " + resultSet.getDouble(3) +
                            "\nname - " + resultSet.getString(4) +
                            "\nperiod in month - " + resultSet.getInt(5) +
                            "\nbank name - " + resultSet.getString(6));
                    loansId.add(resultSet.getInt(1));
                }
            }
            return loansId;
        }
    }

    public static Set<Integer> showInfoAboutAccounts() throws SQLException {
        Set<Integer> accountsId = new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SELECT_ACCOUNT_INFO)) {
            preparedStatement.setInt(1, userID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid - " + resultSet.getInt(1) +
                            "\nbalance - " + resultSet.getInt(2) +
                            "\npercent - " + resultSet.getDouble(3) +
                            "\nname - " + resultSet.getString(4) +
                            "\nperiod in month - " + resultSet.getInt(5) +
                            "\nbank name - " + resultSet.getString(6));
                    accountsId.add(resultSet.getInt(1));
                }
            }
            return accountsId;
        }
    }

    @Override
    public void execute() throws SQLException, IOException {
        if (Objects.isNull(userID)) {
            LOG.info("\nChoose yourself!!!");
        } else {
            LOG.info("\n1 - Info about accounts \n2 - Info about loans");
            String input = INPUT.readLine();
            if (isInteger(input)) {
                if (Integer.parseInt(input) == 1) {
                    showInfoAboutAccounts();
                } else if (Integer.parseInt(input) == 2) {
                    showInfoAboutLoans();
                } else {
                    LOG.info("Choose correct option.");
                }
            }
        }
    }
}
