package com.igor.controller.select;

import com.igor.controller.Controller;
import com.igor.dao.interfaces.GeneralDAO;
import com.igor.model.constant.table.CustomerConsts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.SELECT_ALL_FROM_USER_AND_USER_NAME;
import static com.igor.view.View.*;

public class SelectUsersController implements Controller {
    private static final Logger LOG = LogManager.getLogger(SelectUsersController.class);

    @Override
    public void execute() throws SQLException, IOException {
        LOG.info("ALL USERS: \n");
        Set<Integer> usersId = showUsers(SELECT_ALL_FROM_USER_AND_USER_NAME);
        if (!usersId.isEmpty()) {
            LOG.info("Choose your id: ");
            String input = INPUT.readLine();
            if (checkCustomerID(input)) {
                setUserID(Integer.parseInt(input));
            } else {
                LOG.info("INPUT CORRECT DATA!");
            }
        } else {
            LOG.info("NOBODY");
        }
    }

    private boolean checkCustomerID(String input) throws SQLException {
        if (isInteger(input)) {
            return Integer.parseInt(input) < GeneralDAO.getNewId(CustomerConsts.TABLE_NAME);
        }
        return false;
    }
}

