package com.igor.controller.delete;

import com.igor.controller.select.ShowInfoController;
import com.igor.dao.implementation.BankDao;
import com.igor.dao.implementation.CustomerHasLoanDao;
import com.igor.dao.implementation.LoanDao;
import com.igor.model.entity.CustomerHasLoanCompositePrimaryKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Set;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.SELECT_BANK_ID_FROM_LOAN;
import static com.igor.model.constant.SQLQueryConsts.SELECT_LOAN_INFO_FOR_GIVING_MONEY;
import static com.igor.view.View.isInteger;
import static com.igor.view.View.userID;

public class DeleteLoanController implements DeleteController {
    private static final Logger LOG = LogManager.getLogger(DeleteLoanController.class);

    @Override
    public void execute() throws SQLException, IOException {
        if (Objects.isNull(userID)) {
            LOG.info("\nChoose yourself!!!");
        } else {

            int loanId = getLoanId();
            int bankId = getBankId(loanId, SELECT_BANK_ID_FROM_LOAN);
            new CustomerHasLoanDao().delete(new CustomerHasLoanCompositePrimaryKey(userID, loanId));
            new LoanDao().delete(loanId);
            new BankDao().updateAssets(bankId, new BankDao().getAssetsById(bankId) + getAmountOfMoney(loanId, SELECT_LOAN_INFO_FOR_GIVING_MONEY));
        }
    }

    private int getLoanId() throws SQLException, IOException {
        while (true) {
            LOG.info("Choose loan id: ");
            Set<Integer> currentLoans = ShowInfoController.showInfoAboutLoans();
            String loanIdStr = INPUT.readLine();
            if (isInteger(loanIdStr)) {
                if (currentLoans.contains(Integer.parseInt(loanIdStr))) {
                    return Integer.parseInt(loanIdStr);
                }
            }
        }
    }
}
