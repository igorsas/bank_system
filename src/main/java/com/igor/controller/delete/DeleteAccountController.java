package com.igor.controller.delete;

import com.igor.controller.select.ShowInfoController;
import com.igor.dao.implementation.AccountDao;
import com.igor.dao.implementation.BankDao;
import com.igor.dao.implementation.CustomerHasAccountDao;
import com.igor.model.entity.CustomerHasAccountCompositePrimaryKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Set;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.SQLQueryConsts.*;
import static com.igor.view.View.isInteger;
import static com.igor.view.View.userID;

public class DeleteAccountController implements DeleteController {
    private static final Logger LOG = LogManager.getLogger(DeleteAccountController.class);

    @Override
    public void execute() throws SQLException, IOException {
        if (Objects.isNull(userID)) {
            LOG.info("\nChoose yourself!!!");
        } else {
            int accountId = getAccountId();
            if (checkBank(accountId)) {
                int bankId = getBankId(accountId, SELECT_BANK_ID_FROM_ACCOUNT);
                new BankDao().updateAssets(bankId, new BankDao().getAssetsById(bankId) - getAmountOfMoney(accountId, SELECT_ACCOUNT_INFO_FOR_GIVING_MONEY));
                new CustomerHasAccountDao().delete(new CustomerHasAccountCompositePrimaryKey(userID, accountId));
                new AccountDao().delete(accountId);
            } else {
                LOG.info("Your bank haven't got enough money. Call the police!");
            }
        }
    }

    private int getAccountId() throws SQLException, IOException {
        while (true) {
            LOG.info("Choose account id: ");
            Set<Integer> currentAccounts = ShowInfoController.showInfoAboutAccounts();
            String accountIdStr = INPUT.readLine();
            if (isInteger(accountIdStr)) {
                if (currentAccounts.contains(Integer.parseInt(accountIdStr))) {
                    return Integer.parseInt(accountIdStr);
                }
            }
        }
    }

    private boolean checkBank(int accountId) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(SELECT_ACCOUNT_INFO_FOR_CHECKING_POSSIBILITY_GIVE_MONEY)) {
            preparedStatement.setInt(1, accountId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1) >= resultSet.getInt(2) * (1 + (resultSet.getDouble(3) / (double) 100) * (resultSet.getInt(4) / (double) 12));
                }
            }
        }
        return false;
    }
}
