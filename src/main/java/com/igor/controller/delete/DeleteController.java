package com.igor.controller.delete;

import com.igor.controller.Controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.igor.model.constant.GeneralConst.CONNECTION;

public interface DeleteController extends Controller {
    default int getAmountOfMoney(int accountId, String sqlQuery) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlQuery)) {
            preparedStatement.setInt(1, accountId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return (int) (resultSet.getInt(1) * (1 + (resultSet.getDouble(2) / (double) 100) * (resultSet.getInt(3) / (double) 12)));
                }
            }
        }
        return 0;
    }

    default int getBankId(int loan, String sqlQuery) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlQuery)) {
            preparedStatement.setInt(1, loan);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
            }
        }
        return 0;
    }
}
