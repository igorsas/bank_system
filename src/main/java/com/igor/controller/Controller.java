package com.igor.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;


public interface Controller {
    void execute() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException;
}
